FROM alpine:3.14

ENV DUPLICITY_FULL_IF_OLDER_THAN=1M
ENV MYSQL_PORT=3306

RUN apk add --no-cache mariadb-client duplicity py3-boto3

VOLUME [ "/data" ]

COPY src/scripts ./

ENTRYPOINT [ "./entrypoint.sh" ]