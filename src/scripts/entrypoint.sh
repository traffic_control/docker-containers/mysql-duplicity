#!/bin/sh

WORKING_DIR=`pwd`
cd `dirname "$0"`

if [ ! -d /tmp/duplicity ] ; then
    mkdir /tmp/duplicity
fi

for f in backup_*.sh; do
  sh "$f" 
done

duplicity -V

echo duplicity $DUPLICITY_PARAMETERS \
--verbosity=9 \
--no-encryption \
--allow-source-mismatch \
--full-if-older-than=$DUPLICITY_FULL_IF_OLDER_THAN \
/tmp/duplicity $DUPLICITY_URL

duplicity $DUPLICITY_PARAMETERS \
--verbosity=9 \
--no-encryption \
--allow-source-mismatch \
--full-if-older-than=$DUPLICITY_FULL_IF_OLDER_THAN \
/tmp/duplicity $DUPLICITY_URL

for f in cleanup_*.sh; do
  sh "$f" 
done

cd $WORKING_DIR