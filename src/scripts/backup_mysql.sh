#!/bin/sh

export MYSQL_PWD=$MYSQL_PASSWORD

mariadb-dump \
    --host $MYSQL_HOST \
    --port $MYSQL_PORT \
    --user $MYSQL_USER \
    --all-databases \
    --single-transaction \
    --compress > /tmp/duplicity/backup.sql